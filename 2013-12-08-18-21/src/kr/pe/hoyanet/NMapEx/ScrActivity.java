package kr.pe.hoyanet.NMapEx;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import Parsing.DataGetterSetters;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.content.Context;
//import android.view.ViewGroup;
import android.content.Intent;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
//import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class ScrActivity extends Activity {
	private ArrayList<DataGetterSetters> dataList = new ArrayList<DataGetterSetters>();
	private DataListAdapter adapter;
	private ListView lv;
	private DataGetterSetters data = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scr);
		
		Intent intent = getIntent();
        String gu = intent.getStringExtra("gu");

		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
		
		lv = (ListView)findViewById(R.id.listView1);
		StringBuilder sb = new StringBuilder();
		TextView tv = (TextView)findViewById(R.id.textView1);
		TextView err = (TextView)findViewById(R.id.textView2);
		ImageView imageview = (ImageView)findViewById(R.id.imageView);
		try{
        	URL url = new URL("http://uranus.smu.ac.kr/~201111195/test22.php");
        	HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        	tv.setText("try");
        	if(conn!= null){
        		conn.setConnectTimeout(10000);
        		conn.setUseCaches(false);
		
        		if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
        			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        				     					
        			while (true){
        				String line = br.readLine();
        				if(line == null)
        					break;
        				sb.append(line + "\n");
        			}
        			br.close();	
        		}
        		else{
        			tv.setText("http_not");
        		}
        		conn.disconnect();
        	}        	
        }
        catch(Exception e){
    		tv.setText(e.toString());
    	}
		tv.setText(sb.toString());
	
		String jsonString = sb.toString();  
		//ArrayAdapter<String> adapter;
		
		if (android.os.Build.VERSION.SDK_INT > 9) {
		      StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		      StrictMode.setThreadPolicy(policy);
		    }
		try{
        	JSONArray ja = new JSONArray(jsonString);
        	String listName[] = new String[ja.length()];
        	int done = 0;
        	//String res = "";
        	for (int i = 0; i < ja.length(); i++) {
        		JSONObject jo = ja.getJSONObject(i);
        		if(jo.getString("gu").equals(gu)){
        			//res += "id : " +jo.getString("id") + "\nro_name : " + jo.getString("ro_name") + "\n";
        			//listName[i++] = jo.getString("ro_name");		
        			Log.e(jo.getString("fullAdd"),jo.getString("ro_name"));
        			
        			done = 1;
        			data = new DataGetterSetters();
        			
        			String rnm = jo.getString("ro_name");
        			String nds = jo.getString("fullAdd");
        			Double lat = jo.getDouble("lat");
        			Double lng = jo.getDouble("lng");
        			
        			int like = jo.getInt("lke");
        			
        			data.setName(rnm);	
        			data.setJuso(nds);
        			data.setLike(like);
        			data.setLAT(lat);
        			data.setLNG(lng);
        			
        			
        			dataList.add(data);
        			
        			tv.setText("");
        			//imageButton.setFocusable(false);
        			//lv.setItemsCanFocus(true);
        		} 
        		
        	}
        	//tv.setText(res);
        	if(done == 0)
        			tv.setText("일치하는 데이터가 없스므니다.");
        	
        	adapter = new DataListAdapter(this, dataList);
        	lv.setAdapter(adapter);
		}
		catch(JSONException e){
        	tv.setText("3");
		}

//		imageview.setOnClickListener(new OnClickListener(){
//			public void onClick(View v)
//			{
//				//int like = data.getLike();
//				//like++;//int like = jo.getInt("lke");
//				//data.setLike(like);
//			}
//		});
		lv.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> a, View v, int position, long id){
				Intent intent = new Intent (ScrActivity.this,NaverMap.class);
				intent.putExtra("rnm", dataList.get(position).getName());
				intent.putExtra("lng",dataList.get(position).getLNG());
				intent.putExtra("lat",dataList.get(position).getLAT());
				startActivity(intent);
				
			}
		});
	}
	
	
	
}
	
	
	
	