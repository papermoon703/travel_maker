package kr.pe.hoyanet.NMapEx;

import android.location.Location;
import android.location.LocationManager;
import android.location.LocationListener;
import android.os.Bundle;
import android.content.Context;
import android.graphics.Rect;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nhn.android.maps.NMapActivity;
import com.nhn.android.maps.NMapController;
import com.nhn.android.maps.NMapOverlay;
import com.nhn.android.maps.NMapOverlayItem;
import com.nhn.android.maps.NMapView;
import com.nhn.android.maps.NMapView.OnMapStateChangeListener;
import com.nhn.android.maps.NMapView.OnMapViewTouchEventListener;
import com.nhn.android.maps.maplib.NGeoPoint;
import com.nhn.android.maps.nmapmodel.NMapError;
import com.nhn.android.maps.overlay.NMapPOIdata;
import com.nhn.android.maps.overlay.NMapPOIitem;
import com.nhn.android.maps.overlay.NMapPathData;
import com.nhn.android.maps.overlay.NMapPathLineStyle;
import com.nhn.android.mapviewer.overlay.NMapCalloutOverlay;
import com.nhn.android.mapviewer.overlay.NMapOverlayManager;
import com.nhn.android.mapviewer.overlay.NMapOverlayManager.OnCalloutOverlayListener;
import com.nhn.android.mapviewer.overlay.NMapPOIdataOverlay.OnStateChangeListener;
import com.nhn.android.mapviewer.overlay.NMapPOIdataOverlay;
import com.nhn.android.mapviewer.overlay.NMapPathDataOverlay;

/**
 * 
 * @author Hansol
 * @version 2.5
 */

public class NaverMap extends NMapActivity implements OnMapStateChangeListener, OnMapViewTouchEventListener, OnCalloutOverlayListener, LocationListener {

	//API_KEY
	public static final String API_KEY="d40ff5f326b4f8c22e2c82fe034165ab";
	//네이버 맵 객체
	NMapView mMapView = null;
	//맵 컨트롤러
	NMapController mMapController = null;
	//맵을 추가할 레이아웃
	LinearLayout MapContainer;
	//오버레이의 리소스를 관리하기 위한 객체 할당
	NMapViewerResourceProvider mMapViewerResourceProvider = null;
	//오버레이 관리자
	NMapOverlayManager mOverlayManager;
	OnStateChangeListener onPOIdataStateChangeListener = null;
	
	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//지도 초기화 시작
		//네이버 지도를 넣기 위한 LinearLayout 컴포넌트
		MapContainer = (LinearLayout)findViewById(R.id.map);
		//네이버 지도 객체 생성
		mMapView = new NMapView(this);
		//지도 객체로부터 컨트롤러 추출
		mMapController = mMapView.getMapController();
		//생성된 네이버 지도 객체를 LinearLayout에 추가시킨다.
		setContentView(mMapView);
		//네이버 지도 객체에 APIKEY 지정
		mMapView.setApiKey(API_KEY);
		//지도를 터치할 수 있도록 옵션 활성화
		mMapView.setClickable(true);
		//지도에 대한 상태 변경 이벤트 연결
		mMapView.setOnMapStateChangeListener(this);
		// 확대/축소를 위한 줌 컨트롤러 표시 옵션 활성화
		mMapView.setBuiltInZoomControls(true, null);
		//지도 초기화 끝

		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0,
				(LocationListener) this);
		NMapViewerResourceProvider mMapViewerResourceProvider = null;
		NMapOverlayManager mOverlayManager;
		
		mMapViewerResourceProvider = new NMapViewerResourceProvider(this);
		mOverlayManager = new NMapOverlayManager(this, mMapView, mMapViewerResourceProvider);

		NMapPOIdata poiData = new NMapPOIdata(1, mMapViewerResourceProvider);
		poiData.beginPOIdata(1);
		poiData.addPOIitem(127.0630205, 37.5091300, "Trail",
				NMapPOIflagType.PIN, 0);
		poiData.endPOIdata();
		NMapPOIdataOverlay poiDataOverlay = mOverlayManager
				.createPOIdataOverlay(poiData, null);
		poiDataOverlay.showAllPOIdata(11);
		// 아이템의 선택 상태 or 말풍선 선택되는 경우를 처리하는 이벤트 리스너
		poiDataOverlay.setOnStateChangeListener(onPOIdataStateChangeListener);
		// 오버레이 아이템 클릭시 표시되는 말풍선 오버레이 클래스 NMapCalloutOverlay 이벤트 리스너
		mOverlayManager
				.setOnCalloutOverlayListener((OnCalloutOverlayListener) this);
	}

	/**
	 * Display my Location & Trail position when my Location is changed
	 * @param latitude
	 * @param longitude
	 */
	private void showMyLocation(double latitude, double longitude) {
		NMapViewerResourceProvider mMapViewerResourceProvider = null;
		NMapOverlayManager mOverlayManager;
		
		mMapViewerResourceProvider = new NMapViewerResourceProvider(this);
		mOverlayManager = new NMapOverlayManager(this, mMapView, mMapViewerResourceProvider);
		NGeoPoint myPoint = new NGeoPoint(longitude, latitude);

		NMapPOIdata poiData = new NMapPOIdata(1, mMapViewerResourceProvider);
		poiData.beginPOIdata(2);
		poiData.addPOIitem(127.0630205, 37.5091300, "Pizza 777-111",
				NMapPOIflagType.TO, 0);
		poiData.addPOIitem(myPoint, "현재위치", NMapPOIflagType.FROM, 0);
		poiData.endPOIdata();
		NMapPOIdataOverlay poiDataOverlay = mOverlayManager
				.createPOIdataOverlay(poiData, null);
		poiDataOverlay.showAllPOIdata(11);
		// 아이템의 선택 상태 or 말풍선 선택되는 경우를 처리하는 이벤트 리스너
		poiDataOverlay.setOnStateChangeListener(onPOIdataStateChangeListener);
		// 오버레이 아이템 클릭시 표시되는 말풍선 오버레이 클래스 NMapCalloutOverlay 이벤트 리스너
		mOverlayManager
				.setOnCalloutOverlayListener((OnCalloutOverlayListener) this);
		NMapController controller = mMapView.getMapController();
		controller.animateTo(myPoint);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onLongPress(NMapView arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLongPressCanceled(NMapView arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onScroll(NMapView arg0, MotionEvent arg1, MotionEvent arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSingleTapUp(NMapView arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTouchDown(NMapView arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTouchUp(NMapView arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Called when map animation state changed 
	 * @param arg0, animType, animState
	 */
	@Override
	public void onAnimationStateChange(NMapView arg0, int animType, int animState) {
		// TODO Auto-generated method stub
		//animType : ANIMATION_TYPE_PAN or ANIMATION_TYPE_ZOOM
		//animState : ANIMATION_STATE_STARTED or ANIMATION_STATE_FINISHED
		
	}

	@Override
	public void onMapCenterChange(NMapView arg0, NGeoPoint arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onMapCenterChangeFine(NMapView arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onMapInitHandler(NMapView mapview, NMapError errorInfo) {
		// TODO Auto-generated method stub
		//정상적으로 초기화되면 errorInfo 객체는 null이 전달되며, 초기화 실패 시 errorInfo객체에 에러 원인이 전달된다.
		if (errorInfo == null) {//success
			/*mMapController.setMapCenter(new NGeoPoint(126.978371, 37.5666091),
					11);*/
		} else {//fail
			android.util.Log.e("NMAP",
					"onMapInitHandler: error=" + errorInfo.toString());
		}

	}

	@Override
	public void onZoomLevelChange(NMapView mapview, int level) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public NMapCalloutOverlay onCreateCalloutOverlay(NMapOverlay arg0,
			NMapOverlayItem arg1, Rect arg2) {
		// TODO Auto-generated method stub
		return new NMapCalloutBasicOverlay(arg0, arg1, arg2);
	}
	
	/**
	 * Display text when Overlay is Clicked
	 * @param poiDataoIdataOverlay
	 * @param item
	 */
	public void onCalloutClick(NMapPOIdataOverlay poiDataoIdataOverlay, NMapPOIitem item){
		//[[TEMP]] handle a clikc event of the callout
		
		Toast.makeText(NaverMap.this, "onCalloutClick: " + item.getTitle(), Toast.LENGTH_LONG).show();
	}
	
	public void onFocusChanged(NMapPOIdataOverlay poiDataOverlay, NMapPOIitem item){
		if(item != null){
			Log.i("NMAP", "onFocusChanged: " + item.toString());	
		}else{
			Log.i("NMAP","onFocusChanged: ");
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		double latitude = location.getLatitude();
		double longitude = location.getLongitude();
		
		showMyLocation(latitude, longitude);
		
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}

}
