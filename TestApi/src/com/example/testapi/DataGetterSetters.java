package com.example.testapi;



import java.util.ArrayList;

import android.util.Log;
 
/**
 * 
 * @author JJaewoo
 * @version 1.0
 */
// 데이터 컨트롤에 사용되는  변수와함수들.
public class DataGetterSetters {
    
    private int mOnum;
    private double mX;
    private double mY;

     
    public void setONum(int ONum)
    {
        this.mOnum = ONum;
    }
     
    public int getOnum()
    {
        return mOnum;
    }
       
    public void setX(double X)
    {
        this.mX = X;
    }
    
    public void setY(double Y)
    {
        this.mY = Y;
    }
     
    public double getX()
    {
    	return mX;
    }
    
    public double getY()
    {
    	return mY;
    }

}