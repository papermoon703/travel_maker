package com.example.testapi;

import java.net.URL;
import java.util.ArrayList;
 
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
 
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/**
 * 
 * @author JJaewoo
 * @version 1.0
 */
// 메인함수.
public class ParseTestActivity extends Activity {
    private ArrayList<DataGetterSetters> dataList;
    private DataListAdapter adapter;
    private ListView listView;
     
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        listView = (ListView)findViewById(R.id.main_lv_content);
         
        try {
            //------ Main parse section start ------//
            SAXParserFactory saxPF = SAXParserFactory.newInstance(); 
            SAXParser saxP = saxPF.newSAXParser();
            XMLReader xmlR = saxP.getXMLReader();
            URL url = new URL("http://openapi.seoul.go.kr:8088/6665656c6b313532313132/xml/GeoInfoWalkway/1/1000"); // 오픈 API 컨트롤.
            DataHandler myDataHandler = new DataHandler();
            xmlR.setContentHandler(myDataHandler);
            xmlR.parse(new InputSource(url.openStream()));
            //------ Main parse section end ------//
             
            // load parsing data & View
            dataList = myDataHandler.getData();
            adapter = new DataListAdapter(this, dataList);
            listView.setAdapter(adapter);
             
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}