package Parsing;
import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
 
import android.util.Log;
 
/**
 * 
 * @author JJaewoo
 * @version 1.0
 */

// xml 에서 값을 직접적으로 옮겨 오는 부분.
public class DataHandler extends DefaultHandler {
    private String elementValue = null;
    private Boolean elementOn = false;
    private ArrayList<DataGetterSetters> dataList = new ArrayList<DataGetterSetters>();
    private DataGetterSetters data = null;
     
    public ArrayList<DataGetterSetters> getData()
    {
        return dataList;
    }
         
    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {
        elementOn = true; 
        if(localName.equals("row")){
            data = new DataGetterSetters();
        }       
    }
  
    /**
     * This will be called when the tags of the XML end.
     **/
    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
  
        elementOn = false;
         
        if(localName.equalsIgnoreCase("objectid")){
            data.setONum(Integer.parseInt(elementValue));
        }  else if(localName.equalsIgnoreCase("lat"))
            data.setLAT(Double.parseDouble(elementValue));
            else if(localName.equalsIgnoreCase("lng")){
                data.setLNG(Double.parseDouble(elementValue));
        } else if(localName.equalsIgnoreCase("row")){
            dataList.add(data);
            data = null;
        }
        else
        {
        	// 쓰지 않는 부분 skip 을 위해 꼭 비어있어야함.
        }
        // 추가 정보가 필요하면 else if 로 필요한 정보 추출 가능.
    }
  
    /**
     * This is called to get the tags value
     **/
    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        if (elementOn) {
            elementValue = new String(ch, start, length);
            elementOn = false;
        }
  
    }
     
}