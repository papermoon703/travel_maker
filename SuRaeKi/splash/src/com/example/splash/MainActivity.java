package com.example.splash;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		startActivity(new Intent(this,SplashActivity.class));
		setContentView(R.layout.activity_main);
		Button button1 = (Button)findViewById(R.id.button1); //종로
        Button button2 = (Button)findViewById(R.id.button2); //은평
        Button button3 = (Button)findViewById(R.id.button3); //도봉
        Button button4 = (Button)findViewById(R.id.button4); //노원
        Button button5 = (Button)findViewById(R.id.button5); //성북
        Button button6 = (Button)findViewById(R.id.button6); //강북
        Button button7 = (Button)findViewById(R.id.button7); //서대문
        Button button8 = (Button)findViewById(R.id.button8); //중
        Button button9 = (Button)findViewById(R.id.button9); //중랑
        Button button10 = (Button)findViewById(R.id.button10); //용산
        Button button11= (Button)findViewById(R.id.button11); //마포
        Button button12 = (Button)findViewById(R.id.button12); //동대문
        Button button13 = (Button)findViewById(R.id.button13); //성동
        Button button14 = (Button)findViewById(R.id.button14); //광진
        Button button15 = (Button)findViewById(R.id.button15); //강동
        Button button16 = (Button)findViewById(R.id.button16); //송파
        Button button17 = (Button)findViewById(R.id.button17); //강남
        Button button18 = (Button)findViewById(R.id.button18); //서초
        Button button19 = (Button)findViewById(R.id.button19); //동작
        Button button20 = (Button)findViewById(R.id.button20); //관악
        Button button21 = (Button)findViewById(R.id.button21); //금천
        Button button22 = (Button)findViewById(R.id.button22); //영등포
        Button button23 = (Button)findViewById(R.id.button23); //양촌
        Button button24 = (Button)findViewById(R.id.button24); //구로
        Button button25 = (Button)findViewById(R.id.button25);//강서
        /*
        button1.setOnTouchListener(mTouchEvent);
        button2.setOnTouchListener(mTouchEvent);
        button3.setOnTouchListener(mTouchEvent);
        button4.setOnTouchListener(mTouchEvent);
        button5.setOnTouchListener(mTouchEvent);
        button6.setOnTouchListener(mTouchEvent);
        button7.setOnTouchListener(mTouchEvent);
        button8.setOnTouchListener(mTouchEvent);
        button9.setOnTouchListener(mTouchEvent);
        button10.setOnTouchListener(mTouchEvent);
        button11.setOnTouchListener(mTouchEvent);
        button12.setOnTouchListener(mTouchEvent);
        button13.setOnTouchListener(mTouchEvent);
        button14.setOnTouchListener(mTouchEvent);
        button15.setOnTouchListener(mTouchEvent);
        button16.setOnTouchListener(mTouchEvent);
        button17.setOnTouchListener(mTouchEvent);
        button19.setOnTouchListener(mTouchEvent);
        button20.setOnTouchListener(mTouchEvent);
        button21.setOnTouchListener(mTouchEvent);
        button22.setOnTouchListener(mTouchEvent);
        button23.setOnTouchListener(mTouchEvent);
        button24.setOnTouchListener(mTouchEvent);
        button25.setOnTouchListener(mTouchEvent);
        */
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
