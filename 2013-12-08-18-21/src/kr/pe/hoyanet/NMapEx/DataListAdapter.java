package kr.pe.hoyanet.NMapEx;

import java.util.ArrayList;

import Parsing.DataGetterSetters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
/**
 * 
 * @author JJaewoo
 * @version 1.0
 */
// 이거는 datalist에 있는 받아온 정보를 리스트뷰에 뿌려주는 간단한 어뎁터임
public class DataListAdapter extends BaseAdapter {
    private ArrayList<DataGetterSetters> dataList;
    private Context context;
     
    public DataListAdapter(Context context, ArrayList<DataGetterSetters> dataList)
    {
        this.context = context;
        this.dataList = dataList;
    }
    @Override
    public int getCount() {
        return dataList.size();
    }
 
    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        DataGetterSetters data = dataList.get(position);
                LayoutInflater inflater;
         
        if(v == null){
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.listview_item,null);
        }
         
        if(data!=null){

            //    만약에 DB에 넣을려면 여기서 넣으면 됨     이게 한줄씩 처리해서 값 넣어주는 거라서 내용 싹 지우고 DBINSER 작업하면 될듯 싶음 
        	
            TextView title = (TextView)v.findViewById(R.id.Name);
            TextView artist = (TextView)v.findViewById(R.id.Juso);
            //TextView country = (TextView)v.findViewById(R.id.main_activity_LNG);
            title.setText(""+data.getName());
            artist.setText(""+data.getJuso());
            //country.setText(""+data.getLAT());
        }
        return v;
    }
}