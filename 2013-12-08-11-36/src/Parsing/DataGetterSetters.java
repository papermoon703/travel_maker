package Parsing;


import java.util.ArrayList;

import android.util.Log;
 
/**
 * 
 * @author JJaewoo, KyoungAh
 * @version 4.0
 */
// 데이터 컨트롤에 사용되는  변수와함수들.
public class DataGetterSetters {
    
    private int mOnum;
    private double LAT;
    private double LNG;
    private String Juso;
    private String Name;
    private int Like;
    private int ID;
    private int POP;
    private String GU;
    
    public void setID(int i){
    	this.ID = i;
    }
    public int getID(){
    	return ID;
    }
    
    public void setName(String N){
    	//Log.e("","여기까진 오니?11");
    	this.Name = N;
    }
    public String getName(){
           return Name;
    }
    
    public void setJuso(String J){
        this.Juso = J;
    }
    public String getJuso(){
        return Juso;
    }
    
    public void setLike(int l){
    	this.Like = l;
    }
    public int getLike(){
    	return Like;
    }
    
    public void setGU(String gu){
    	this.GU = gu;
    }
    public String getGU(){
    	return GU;
    }
    
    public void setPOP(int p){
    	this.POP = p;
    }
    public int getPOP(){
    	return POP;
    }
    
    public void setONum(int ONum){
        this.mOnum = ONum;
    }
     
    public int getOnum(){
        return mOnum;
    }
       
    public void setLAT(double LAT){
        this.LAT = LAT;
    }
    
    public void setLNG(double LNG){
        this.LNG = LNG;
    }
     
    public double getLAT(){
    	return LAT;
    }
    
    public double getLNG(){
    	return LNG;
    }

}