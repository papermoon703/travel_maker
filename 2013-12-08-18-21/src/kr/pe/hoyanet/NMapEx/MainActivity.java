package kr.pe.hoyanet.NMapEx;


import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import Parsing.DataGetterSetters;
import Parsing.DataHandler;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;

/**
 * 
 * @author Seungjun
 * @version 2.3
 */

public class MainActivity extends Activity {
	private ArrayList<DataGetterSetters> dataList;
	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		startActivity(new Intent(this,SplashActivity.class));
		setContentView(R.layout.main);
		
		//////////////////////////////////////////////////////////////////////////////////////
		
		
		 if (android.os.Build.VERSION.SDK_INT > 9) {
		      StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		      StrictMode.setThreadPolicy(policy);
		    }// 쓰레드 문제 해결해주는 이프문  // 여기서 에러나면 프로젝트 클린하면 됩니다.
        /*
		try {
            //------ Main parse section start ------//
            SAXParserFactory saxPF = SAXParserFactory.newInstance(); 
            SAXParser saxP = saxPF.newSAXParser();
            XMLReader xmlR = saxP.getXMLReader();
            URL url = new URL("http://openapi.seoul.go.kr:8088/6665656c6b313532313132/xml/GeoInfoWalkwayWGS/1/1000"); // 오픈 API 컨트롤.
            DataHandler myDataHandler = new DataHandler();
            xmlR.setContentHandler(myDataHandler);
            xmlR.parse(new InputSource(url.openStream()));
            //------ Main parse section end ------//
             
            // load parsing data & View
            dataList = myDataHandler.getData();
             
        } catch (Exception e) {
            System.out.println(e);
        }
		
		Log.e("", ""+dataList.get(1).getLAT());
		*/
		
		
		//////////////////////////////////////////////////////////////////
		
		
		
		
		
		
		
		
		Button button1 = (Button)findViewById(R.id.button1); //종로
		
        Button button2 = (Button)findViewById(R.id.button2); //은평
        Button button3 = (Button)findViewById(R.id.button3); //도봉
        Button button4 = (Button)findViewById(R.id.button4); //노원
        Button button5 = (Button)findViewById(R.id.button5); //성북
        Button button6 = (Button)findViewById(R.id.button6); //강북
        Button button7 = (Button)findViewById(R.id.button7); //서대문
        Button button8 = (Button)findViewById(R.id.button8); //중
        Button button9 = (Button)findViewById(R.id.button9); //중랑
        Button button10 = (Button)findViewById(R.id.button10); //용산
        Button button11 = (Button)findViewById(R.id.button11); //마포
        Button button12 = (Button)findViewById(R.id.button12); //동대문
        Button button13 = (Button)findViewById(R.id.button13); //성동
        Button button14 = (Button)findViewById(R.id.button14); //광진
        Button button15 = (Button)findViewById(R.id.button15); //강동
        Button button16 = (Button)findViewById(R.id.button16); //송파
        Button button17 = (Button)findViewById(R.id.button17); //강남
        Button button18 = (Button)findViewById(R.id.button18); //서초
        Button button19 = (Button)findViewById(R.id.button19); //동작
        Button button20 = (Button)findViewById(R.id.button20); //관악
        Button button21 = (Button)findViewById(R.id.button21); //금천
        Button button22 = (Button)findViewById(R.id.button22); //영등포
        Button button23 = (Button)findViewById(R.id.button23); //양촌
        Button button24 = (Button)findViewById(R.id.button24); //구로
        Button button25 = (Button)findViewById(R.id.button25);//강서
        
        button1.setOnClickListener(new OnClickListener()
        {
            // 클릭 이벤트를 처리한다.
            public void onClick(View v)
            {
                // 무엇을 할지 정의 하는 인텐트를 생성한다.
                // FirstActivity에서 SecondActivity로 이동 할것을 정의하였다.
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "종로구");
                // 인텐트에 있는 정보대로 액티비티를 시작한다.
                startActivity(intent);
        		//setContentView(R.layout.main);
            
            }
        });
        button2.setOnClickListener(new OnClickListener()
        {
        	public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "은평구");
                startActivity(intent);
            }
        });
        button3.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "도봉구");
                startActivity(intent);
            }
        });
        button4.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "노원구");
                startActivity(intent);
            }
        });
        button5.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "성북구");
                startActivity(intent);
            }
        });
        button6.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "강북구");
                startActivity(intent);
            }
        });
        button7.setOnClickListener(new OnClickListener()
        {
        	public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "서대문구");
                startActivity(intent);
            }
        });
        button8.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "중구");
                startActivity(intent);
            }
        });
        button9.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "중랑구");
                startActivity(intent);
            }
        });
        button10.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "용산구");
                startActivity(intent);
            }
        });
        button11.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "마포구");
                startActivity(intent);
            }
        });
        button12.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "동대문구");
                startActivity(intent);
            }
        });
        button13.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "성동구");
                startActivity(intent);
            }
        });
        button14.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "광진구");
                startActivity(intent);
            }
        });
        button15.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "강동구");
                startActivity(intent);
            }
        });
        button16.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "송파구");
                startActivity(intent);
            }
        });
        button17.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "강남구");
                startActivity(intent);
            }
        });
        button18.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "서초구");
                startActivity(intent);
            }
        });
        button19.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "동작구");
                startActivity(intent);
            }
        });
        button20.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "관악구");
                startActivity(intent);
            }
        });
        button21.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "금천구");
                startActivity(intent);
            }
        });
        button22.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "영등포구");
                startActivity(intent);
            }
        });
        button23.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "양촌구");
                startActivity(intent);
            }
        });
        button24.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "구로구");
                startActivity(intent);
            }
        });
        button25.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
            	Intent intent = new Intent(MainActivity.this, ScrActivity.class);
                intent.putExtra("gu", "강서구");
                startActivity(intent);
            }
        });
        
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
