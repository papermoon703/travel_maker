package Parsing;



import java.util.ArrayList;

import android.util.Log;
 
/**
 * 
 * @author JJaewoo
 * @version 1.0
 */
// 데이터 컨트롤에 사용되는  변수와함수들.
public class DataGetterSetters {
    
    private int mOnum;
    private double mLAT;
    private double mLNG;

     
    public void setONum(int ONum)
    {
        this.mOnum = ONum;
    }
     
    public int getOnum()
    { 
        return mOnum;
    }
       
    public void setLAT(double LAT)
    {
        this.mLAT = LAT;
    }
    
    public void setLNG(double LNG)
    {
        this.mLNG = LNG;
    }
     
    public double getLAT()
    {
    	return mLAT;
    }
    
    public double getLNG()
    {
    	return mLNG;
    }

}