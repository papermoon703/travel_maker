package kr.pe.hoyanet.NMapEx;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import Parsing.DataGetterSetters;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.StrictMode;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nhn.android.maps.NMapActivity;
import com.nhn.android.maps.NMapController;
import com.nhn.android.maps.NMapOverlay;
import com.nhn.android.maps.NMapOverlayItem;
import com.nhn.android.maps.NMapView;
import com.nhn.android.maps.NMapView.OnMapStateChangeListener;
import com.nhn.android.maps.NMapView.OnMapViewTouchEventListener;
import com.nhn.android.maps.maplib.NGeoPoint;
import com.nhn.android.maps.nmapmodel.NMapError;
import com.nhn.android.maps.overlay.NMapPOIdata;
import com.nhn.android.maps.overlay.NMapPOIitem;
import com.nhn.android.maps.overlay.NMapPathData;
import com.nhn.android.maps.overlay.NMapPathLineStyle;
import com.nhn.android.mapviewer.overlay.NMapCalloutOverlay;
import com.nhn.android.mapviewer.overlay.NMapOverlayManager;
import com.nhn.android.mapviewer.overlay.NMapOverlayManager.OnCalloutOverlayListener;
import com.nhn.android.mapviewer.overlay.NMapPOIdataOverlay.OnStateChangeListener;
import com.nhn.android.mapviewer.overlay.NMapPOIdataOverlay;
import com.nhn.android.mapviewer.overlay.NMapPathDataOverlay;

//import Parsing.DataGetterSetters;
/**
 * 
 * @author Hansol, KyoungAh
 * @version 4.0
 */

public class NaverMap extends NMapActivity implements OnMapStateChangeListener, OnMapViewTouchEventListener, OnCalloutOverlayListener, LocationListener {
	
	int like=1;
	int pop;
	String guName = "";
	
	private int index;
	//API_KEY
	public static final String API_KEY="d40ff5f326b4f8c22e2c82fe034165ab";
	//네이버 맵 객체
	NMapView mMapView = null;
	//맵 컨트롤러
	NMapController mMapController = null;
	//맵을 추가할 레이아웃
	LinearLayout MapContainer;
	//오버레이의 리소스를 관리하기 위한 객체 할당
	NMapViewerResourceProvider mMapViewerResourceProvider = null;
	//오버레이 관리자
	NMapOverlayManager mOverlayManager;
	OnStateChangeListener onPOIdataStateChangeListener = null;
	
	
	
	
	/**
	 * Called when the list has been clicked. 
	 * This class have intent pattern so bring the values about SanCheckRo.
	 * This shows the naver map with a pin.
	 * @param savedInstanceState
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//지도 초기화 시작
		//네이버 지도를 넣기 위한 LinearLayout 컴포넌트
		//MapContainer = (LinearLayout)findViewById(R.id.map);
		//네이버 지도 객체 생성
		//mMapView = new NMapView(this);
		setContentView(R.layout.activity_main);
		mMapView = (NMapView)findViewById(R.id.mapview);
		//지도 객체로부터 컨트롤러 추출
		mMapController = mMapView.getMapController();
		//생성된 네이버 지도 객체를 LinearLayout에 추가시킨다.
		//setContentView(mMapView);
		//네이버 지도 객체에 APIKEY 지정
		mMapView.setApiKey(API_KEY);
		//지도를 터치할 수 있도록 옵션 활성화
		mMapView.setClickable(true);
		//지도에 대한 상태 변경 이벤트 연결
		mMapView.setOnMapStateChangeListener(this);
		// 확대/축소를 위한 줌 컨트롤러 표시 옵션 활성화
		mMapView.setBuiltInZoomControls(true, null);
		//지도 초기화 끝
		
		

		
		Intent intent = getIntent();
		//String rnm = intent.getStringExtra("rnm");
		
		String trailname = intent.getStringExtra("rnm");
		double latitude = intent.getDoubleExtra("lat", 0);
		double longtitude = intent.getDoubleExtra("lng",0);
		//int like = intent.getIntExtra("like",0);
		int id = intent.getIntExtra("id",0);
		String gu = intent.getStringExtra("gu");
		
		index = id;
		guName = gu;
		
		ImageButton imagebutton = (ImageButton)findViewById(R.id.imageButton);
		
		/**
		 * Called when the 'like' icon has been clicked. 
		 * This class jub-geun to the database and bring the values about SanSheckRo. HaHaHa.
		 * @param savedInstanceState
		 */
		
		imagebutton.setOnClickListener(new OnClickListener(){
			public void onClick(View v)
			{
				
				StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
				StringBuilder sb = new StringBuilder();
				try{
		        	URL url = new URL("http://uranus.smu.ac.kr/~201111195/like_query.php?"+"id="+index);
		        	HttpURLConnection conn = (HttpURLConnection)url.openConnection();	//연결함
		        	
		        	if(conn!= null){
		        		conn.setConnectTimeout(10000);
		        		conn.setUseCaches(false);
		        		
		        		if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
		        			
		        			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));//거기 조건문 수행 결과
		        			Log.e("","br = "+br);
		        			while (true){
		        				String line = br.readLine();
		        				if(line == null)
		        					break;
		        				sb.append(line);
		        			}
		        			br.close();	
		        			Log.e("", "line: "+sb);
		        		}

		        		conn.disconnect();
		        	}        	
		        }
		        catch(Exception e){}
				
				
				String jsonString = sb.toString();        
			    
				
				
				
		        try{
		        	JSONArray ja = new JSONArray(jsonString);
		        	
		        	for (int i = 0; i < ja.length(); i++) {
		        		JSONObject jo = ja.getJSONObject(i);		 
		        		like = jo.getInt("lke");
		        		//pop = jo.getInt("pop");
		        		//gu = jo.getString("gu");
		        	}		        	      	
		        }
		        catch(JSONException e){} 
				try {
					like = like+1;
					URL url2 = new URL("http://uranus.smu.ac.kr/~201111195/change_like.php?"+ "id="+index + "&lke="+like);
					HttpURLConnection conn2 = (HttpURLConnection)url2.openConnection();
					if(conn2!= null){
		        		conn2.setConnectTimeout(10000);
		        		conn2.setUseCaches(false);

		        		if(conn2.getResponseCode() == HttpURLConnection.HTTP_OK){
		        			BufferedReader br = new BufferedReader(new InputStreamReader(conn2.getInputStream()));//거기 조건문 수행 결과
		        			Log.e("","br = "+br);
		        			while (true){
		        				String line = br.readLine();
		        				if(line == null)
		        					break;
		        				sb.append(line);
		        			}
		        			br.close();	
		        			Log.e("", "line: "+sb);
		        		}
		        		conn2.disconnect();
		        	}        	
				} catch (Exception e) {}
	        	

				try{
		        	JSONArray ja = new JSONArray(jsonString);
		        	
		        	for (int i = 0; i < ja.length(); i++) {
		        		JSONObject jo = ja.getJSONObject(i);		 
		        		pop = jo.getInt("pop");
		        		guName = jo.getString("gu");
		        		Log.e("","guname1: "+guName);
		        	}		        	      	
		        }
		        catch(JSONException e){}
				
				
				StringBuilder sb2 = new StringBuilder();
				try {
					
					pop = pop+1;
					Log.e("","guname2: "+guName);
					URL url3 = new URL("http://uranus.smu.ac.kr/~201111195/change_pop2.php?"+ "pop="+pop + "&gu="+guName);
					HttpURLConnection conn3 = (HttpURLConnection)url3.openConnection();
					if(conn3!= null){
		        		conn3.setConnectTimeout(10000);
		        		conn3.setUseCaches(false);

		        		if(conn3.getResponseCode() == HttpURLConnection.HTTP_OK){
		        			BufferedReader br = new BufferedReader(new InputStreamReader(conn3.getInputStream()));//거기 조건문 수행 결과
		        			Log.e("","br = "+br);
		        			while (true){
		        				String line = br.readLine();
		        				if(line == null)
		        					break;
		        				sb2.append(line);
		        			}
		        			br.close();	
		        			Log.e("", "line: "+sb2);
		        		}
		        		conn3.disconnect();
		        	}        	
				} catch (Exception e) {}
			}
		});
		
		LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0,
				(LocationListener) this);
		NMapViewerResourceProvider mMapViewerResourceProvider = null;
		NMapOverlayManager mOverlayManager;
		
		mMapViewerResourceProvider = new NMapViewerResourceProvider(this);
		mOverlayManager = new NMapOverlayManager(this, mMapView, mMapViewerResourceProvider);

		NMapPOIdata poiData = new NMapPOIdata(1, mMapViewerResourceProvider);
		poiData.beginPOIdata(1);
		poiData.addPOIitem(longtitude, latitude, trailname,
				NMapPOIflagType.PIN, 0);
		poiData.endPOIdata();
		NMapPOIdataOverlay poiDataOverlay = mOverlayManager
				.createPOIdataOverlay(poiData, null);
		poiDataOverlay.showAllPOIdata(13);
		// 아이템의 선택 상태 or 말풍선 선택되는 경우를 처리하는 이벤트 리스너
		poiDataOverlay.setOnStateChangeListener(onPOIdataStateChangeListener);
		// 오버레이 아이템 클릭시 표시되는 말풍선 오버레이 클래스 NMapCalloutOverlay 이벤트 리스너
		mOverlayManager
				.setOnCalloutOverlayListener((OnCalloutOverlayListener) this);
		//testPOldataOverlay();
		
	}

	/**
	 * Display my Location & Trail position when my Location is changed
	 * @param latitude
	 * @param longitude
	 */
	private void showMyLocation(double latitude, double longitude) {
		NMapViewerResourceProvider mMapViewerResourceProvider = null;
		NMapOverlayManager mOverlayManager;
		
		mMapViewerResourceProvider = new NMapViewerResourceProvider(this);
		mOverlayManager = new NMapOverlayManager(this, mMapView, mMapViewerResourceProvider);
		NGeoPoint myPoint = new NGeoPoint(longitude, latitude);

		NMapPOIdata poiData = new NMapPOIdata(1, mMapViewerResourceProvider);
		poiData.beginPOIdata(2);
		poiData.addPOIitem(127.0630205, 37.5091300, "Pizza 777-111",
				NMapPOIflagType.TO, 0);
		poiData.addPOIitem(myPoint, "현재위치", NMapPOIflagType.FROM, 0);
		poiData.endPOIdata();
		NMapPOIdataOverlay poiDataOverlay = mOverlayManager
				.createPOIdataOverlay(poiData, null);
		poiDataOverlay.showAllPOIdata(11);
		// 아이템의 선택 상태 or 말풍선 선택되는 경우를 처리하는 이벤트 리스너
		poiDataOverlay.setOnStateChangeListener(onPOIdataStateChangeListener);
		// 오버레이 아이템 클릭시 표시되는 말풍선 오버레이 클래스 NMapCalloutOverlay 이벤트 리스너
		mOverlayManager
				.setOnCalloutOverlayListener((OnCalloutOverlayListener) this);
		NMapController controller = mMapView.getMapController();
		controller.animateTo(myPoint);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onLongPress(NMapView arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLongPressCanceled(NMapView arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onScroll(NMapView arg0, MotionEvent arg1, MotionEvent arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSingleTapUp(NMapView arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTouchDown(NMapView arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTouchUp(NMapView arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Called when map animation state changed 
	 * @param arg0, animType, animState
	 */
	@Override
	public void onAnimationStateChange(NMapView arg0, int animType, int animState) {
		// TODO Auto-generated method stub
		//animType : ANIMATION_TYPE_PAN or ANIMATION_TYPE_ZOOM
		//animState : ANIMATION_STATE_STARTED or ANIMATION_STATE_FINISHED
		
	}

	@Override
	public void onMapCenterChange(NMapView arg0, NGeoPoint arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onMapCenterChangeFine(NMapView arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onMapInitHandler(NMapView mapview, NMapError errorInfo) {
		// TODO Auto-generated method stub
		//정상적으로 초기화되면 errorInfo 객체는 null이 전달되며, 초기화 실패 시 errorInfo객체에 에러 원인이 전달된다.
		if (errorInfo == null) {//success
			/*mMapController.setMapCenter(new NGeoPoint(126.978371, 37.5666091),
					11);*/
		} else {//fail
			android.util.Log.e("NMAP",
					"onMapInitHandler: error=" + errorInfo.toString());
		}

	}

	@Override
	public void onZoomLevelChange(NMapView mapview, int level) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public NMapCalloutOverlay onCreateCalloutOverlay(NMapOverlay arg0,
			NMapOverlayItem arg1, Rect arg2) {
		// TODO Auto-generated method stub
		return new NMapCalloutBasicOverlay(arg0, arg1, arg2);
	}
	
	/**
	 * Display text when Overlay is Clicked
	 * @param poiDataoIdataOverlay
	 * @param item
	 */
	public void onCalloutClick(NMapPOIdataOverlay poiDataoIdataOverlay, NMapPOIitem item){
		//[[TEMP]] handle a clikc event of the callout
		
		Toast.makeText(NaverMap.this, "onCalloutClick: " + item.getTitle(), Toast.LENGTH_LONG).show();
	}
	
	public void onFocusChanged(NMapPOIdataOverlay poiDataOverlay, NMapPOIitem item){
		if(item != null){
			Log.i("NMAP", "onFocusChanged: " + item.toString());	
		}else{
			Log.i("NMAP","onFocusChanged: ");
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		double latitude = location.getLatitude();
		double longitude = location.getLongitude();
		
		showMyLocation(latitude, longitude);
		
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}

}
