package Parsing;


import java.util.ArrayList;

import android.util.Log;
 
/**
 * 
 * @author JJaewoo
 * @version 1.0
 */
// 데이터 컨트롤에 사용되는  변수와함수들.
public class DataGetterSetters {
    
    private int mOnum;
    private double mLAT;
    private double mLNG;
    private String Juso;
    private String Name;
    private int Like;
    private int ID;
    
    public void setID(int i)
    {
    	this.ID = i;
    }
    public int getID()
    {
    	return ID;
    }
    public void setName(String N)
    {
    	Log.e("","여기까진 오니?11");
    	this.Name = N;
        
    }
    
    public void setLike(int l) 
    {
    	this.Like = l;
    }
    
    public String getName()
    { 
        return Name;
    }
    
    public void setJuso(String J)
    {
        this.Juso = J;
    }
     
    public String getJuso()
    { 
        return Juso;
    }
     
    public int getLike() 
    {
    	return Like;
    }
    public void setONum(int ONum)
    {
        this.mOnum = ONum;
    }
     
    public int getOnum()
    { 
        return mOnum;
    }
       
    public void setLAT(double LAT)
    {
        this.mLAT = LAT;
    }
    
    public void setLNG(double LNG)
    {
        this.mLNG = LNG;
    }
     
    public double getLAT()
    {
    	return mLAT;
    }
    
    public double getLNG()
    {
    	return mLNG;
    }

}